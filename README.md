框架结构：![输入图片说明](https://gitee.com/uploads/images/2018/0109/224728_734ed557_1335015.png "屏幕截图.png")

IRQQMain:![输入图片说明](https://gitee.com/uploads/images/2018/0109/230507_33d3bf39_1335015.png "屏幕截图.png")
            这个类里面是4个导出函数  是IRQQ框架调用 与我们插件互动的关键

IRQQApi:![输入图片说明](https://gitee.com/uploads/images/2018/0109/230613_9fdb8680_1335015.png "屏幕截图.png")
            这是调用IRQQ框架给出的API  由于我比较懒  可能会更新不及时  一切可用API以IRQQ官方为主

FormMain: 就是一个窗体  他会在插件写好直接 IRQQ框架调用 启动插件之后 右击设置 会弹出这个窗体  可以做配置等   
            这个是在IRQQMain->IR_SetUp函数调用的

使用注意：
            1.![输入图片说明](https://gitee.com/uploads/images/2018/0109/231029_05c5506a_1335015.png "屏幕截图.png")
                在IRQQMain下IR_Create函数下 有一个插件名称  需要跟生成出来的DLL文件名称一致
                如：我的这里是 C#Demo       DLL 名称应该是 C#Demo.IR.dll
            2.![输入图片说明](https://gitee.com/uploads/images/2018/0109/231217_884e8216_1335015.png "屏幕截图.png")
                如果不想一直手动修改  可以右击我的项目->属性->应用程序->修改程序集名称
            3.![输入图片说明](https://gitee.com/uploads/images/2018/0109/231427_d14d5712_1335015.png "屏幕截图.png")
                如果生成失败 说找不到目录问题 可以右击我的项目->属性->生成->修改输出路径
            4.目标框架 最好不要改动 这是DLLExport的问题  我当时测试的时候 好像只有x86用DLLExport管用 可以测试下 

插件调用流程:   启动IRQQ框架-> 框架会在固定目录下找到我们放的DLL文件 -> 调用DLL文件的4个导出函数

技术难点：由于微软大大 没有提供 导出函数（DLLExport）所以 引用了一个github大佬的类库 链接：https://github.com/3F/DllExport

如有问题 可以加群  我的C#Q群：608188505   IR官方Q群：476715371    

大家如果觉得好 可以帮忙在码云上点个小星星 如果有条件可以捐赠一下 支持一下我 